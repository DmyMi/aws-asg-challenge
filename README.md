# AWS Challenge

## Description

### Application

Small Java application that shows funny gif :).

Application runs of PORT `8080` (*important for load balancer*)

### Task
Create a small scalable infrastructure that consists of:
- Load balancer and Target group (targeting port `8080`)
- Auto Scaling Group (using `t2.small` or `t2.medium` type)

The autoscaling group *must use custom image* with this application.

To create the image you must first:
1. Create a new EC2 instance using `t2.small` (or even `t2.medium`), `micro` won't work :)
2. SSH into it
3. Clone this repository
4. Install `JDK 11` (installation process is different for different OS, check docs or google :))
5. Build the application by going into `AWSChallenge` folder and executing `./gradlew bootJar`
6. Copy the JAR file from `AWSChallenge/build/libs/AWSChallenge-0.0.1.jar` to `/opt/application.jar`
7. Copy the `AWSChallenge/sample.service` SystemD unit to `/etc/systemd/system/sample.service`
8. Enable Application autostart by running `sudo systemctl enable --now sample.service`
9. Create the image from this VM.